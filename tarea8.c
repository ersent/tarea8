#include <stdio.h>
int main(int argc, char const *argv[]) {

  int a;
  float b;
  char c;
  long int d;

  printf("El tamaño de int es: %ld bytes\n", sizeof(a));
  printf("El tamaño de float es: %ld bytes\n", sizeof(b));
  printf("El tamaño de char es: %ld bytes\n", sizeof(c));
  printf("El tamaño de long int es: %ld bytes\n", sizeof(d));
  
  return 0;
}
